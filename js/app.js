// Importar el SDK de Firebase
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.0/firebase-app.js";
import { getDatabase, ref, set, onValue, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.12.0/firebase-database.js";

// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyC645KkUlpNL4-SNR5CD7mT6f2cZK18EzU",
  authDomain: "bdprogw.firebaseapp.com",
  databaseURL: "https://bdprogw-default-rtdb.firebaseio.com",
  projectId: "bdprogw",
  storageBucket: "bdprogw.appspot.com",
  messagingSenderId: "770598850915",
  appId: "1:770598850915:web:d4b76a5fd0962faf9e8356"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

// Declarar variables globales
var numSerie = "";
var marca = "";
var modelo = "";
var descripcion = "";
var urlImag = "";

// Funciones
function leerInput() {
  numSerie = document.getElementById('txtNumSerie').value;
  marca = document.getElementById('txtMarca').value;
  modelo = document.getElementById('txtModelo').value;
  descripcion = document.getElementById('txtDescripcion').value;
  urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje(mensaje) {
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';

  setTimeout(() => {
    mensajeElement.style.display = 'none';
  }, 1000);
}

// Agregar producto a la base de datos
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto(event) {
  event.preventDefault(); // Evitar el envío del formulario
  
  leerInput();
  
  // Validar
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
    mostrarMensaje("Faltaron datos por capturar");
    return;
  }

  set(ref(db, 'Automoviles/' + numSerie), {
    numSerie: numSerie,
    marca: marca,
    modelo: modelo,
    descripcion: descripcion,
    urlImag: urlImag
  }).then(() => {
    alert("Se agregó con éxito");
  }).catch((error) => {
    alert("Ocurrió un error");
  });

  Listarproductos();
}

function Listarproductos() {
  const dbRef = ref(db, 'Automoviles');
  const tabla = document.getElementById('tablaProductos');
  const tbody = tabla.querySelector('tbody');
  tbody.innerHTML = '';

  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val();
      const fila = document.createElement('tr');

      const celdaCodigo = document.createElement('td');
      celdaCodigo.textContent = childKey;
      fila.appendChild(celdaCodigo);

      var celdaNombre = document.createElement('td');
      celdaNombre.textContent = data.marca;
      fila.appendChild(celdaNombre);

      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = data.modelo;
      fila.appendChild(celdaPrecio);

      var celdaCantidad = document.createElement('td');
      celdaCantidad.textContent = data.descripcion;
      fila.appendChild(celdaCantidad);

      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.urlImag;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);

      tbody.appendChild(fila);
    });
  }, { onlyOnce: true });
}

function buscarAutomovil() {
  numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
    mostrarMensaje("Faltó capturar el número de serie");
    return;
  }

  const dbref = ref(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()) {
      marca = snapshot.val().marca;
      modelo = snapshot.val().modelo;
      descripcion = snapshot.val().descripcion;
      urlImag = snapshot.val().urlImag;
      escribirInputs();
    } else {
      limpiarInputs();
      mostrarMensaje("No se encontró el registro");
    }
  });
}

function actualizarAutomovil() {
  leerInput();
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
    mostrarMensaje("Favor de capturar toda la información.");
    return;
  }
  alert("Actualizar");
  update(ref(db, 'Automoviles/' + numSerie), {
    numSerie: numSerie,
    marca: marca,
    modelo: modelo,
    descripcion: descripcion,
    urlImg: urlImag
  }).then(() => {
    mostrarMensaje("Se actualizó con éxito.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    mostrarMensaje("Ocurrió un error: " + error);
  });
}

function eliminarAutomovil() {
  let numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
    mostrarMensaje("No se ingresó un código válido.");
    return;
  }
  const dbref = ref(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()) {
      remove(ref(db, 'Automoviles/' + numSerie)).then(() => {
        mostrarMensaje("Producto eliminado con éxito.");
        limpiarInputs();
        Listarproductos();
      }).catch((error) => {
        mostrarMensaje("Ocurrió un error al eliminar el producto: " + error);
      });
    } else {
      limpiarInputs();
      mostrarMensaje("El producto con ID " + numSerie + " no existe.");
    }
  });
}

function escribirInputs() {
  document.getElementById('txtMarca').value = marca;
  document.getElementById('txtModelo').value = modelo;
  document.getElementById('txtDescripcion').value = descripcion;
  document.getElementById('txtUrl').value = urlImag;
}

function limpiarInputs() {
  document.getElementById('txtNumSerie').value = '';
  document.getElementById('txtMarca').value = '';
  document.getElementById('txtModelo').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

// Event Listeners
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarAutomovil);

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);
